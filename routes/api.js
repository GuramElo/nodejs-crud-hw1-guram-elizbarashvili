var express = require('express');
var router = express.Router();

let {
    getAllRecords,
    updateRecord,
    deleteRecord,
    addRecord,
    getRecordById
} = require('../services/user_records');

router.get('/', (req, res) => {
    res.render('crud', {Records: getAllRecords()})
});
router.delete('/delete', (req, res) => {
    deleteRecord(req.body);
    res.send({
        status: "ok",
        statusCode: 200
    });
});
router.put('/put', (req, res) => {
    updateRecord(req.body);
    res.send({
        status: "ok",
        statusCode: 200
    });
});
router.post('/post', (req, res) => {
    addRecord(req.body);
    res.send({
        status: "ok",
        statusCode: 200
    });
});
router.get('/:id', (req, res) => {
    res.render('search_by_id', {Record: getRecordById(req.params.id)})
});

module.exports = router;