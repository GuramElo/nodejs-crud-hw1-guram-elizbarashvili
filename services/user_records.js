let records = new Array(8);
for(let i=1 ; i<9; i++) {
    records[i-1] = 
        {
            id: i,
            name: "firstname" + i,
            company: "company" + i,
            code: "coded" + i,
            date: (new Date()).toDateString(),
            city: "tbilisi" + i,
            country: "Georgia" + i
        };
}
const getAllRecords = () => {
    return records;
}
const updateRecord = (record) => {
    for(let i in records) {
        if(records[i].id == record.id) {
            records[i] = record;
            break;
        }
    }
}
const  deleteRecord = (record) => {
    records.splice([records.findIndex((singleRecord) => {
        return singleRecord.id == record.id;
    })], 1);
    console.log(records);
}
const addRecord = (record) => {
    records.push({
        ...record,
        id: (Number(records[records.length - 1]?.id ?? 0) + 1)
    });
}
function getRecordById(id) {
    return records.find((record) => {
        return record.id == id;
    });
}
const allFunctions = {
    getAllRecords,
    updateRecord,
    updateRecord,
    deleteRecord,
    addRecord,
    getRecordById
}
module.exports = allFunctions;